package ca.campbell.simplecalc

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

class MainActivity : Activity() {
    // Get a handle on the views in the UI
    // lateinit allows us to avoid using null initialization & !! & ?
    // but we must be sure to init before use otherwise
    // it acts as if !! was used & crashes on null value
    private lateinit var etNumber1: EditText
    private lateinit var result: TextView
    internal var num1: Double = 0.toDouble()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        etNumber1 = findViewById(R.id.num1) as EditText
        }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    fun writeToScreen(v: View) {
        etNumber1.text.append(v.tag.toString())
    }
    fun correctScreen(v:View){
        if(etNumber1.text.length<1){
            return
        }
        etNumber1.text.delete(etNumber1.text.length-1,etNumber1.text.length)
    }
    fun clearScreen(v:View){
        etNumber1.text.clear()
    }
    fun equalsButton(v:View){
        var previousScreen=etNumber1.text.toString()
        etNumber1.text.clear()
        etNumber1.append(calcString(previousScreen).toString())
    }
    fun calcString(s:String):Double{
        var left=0;
        var priorityOperators=0;
        var positionLargestBracket=0;
        for(c in s){
            if(c=='('){
                left++
            }
            else if(c==')'){
                left--;
            }
            else if(isOperator(c)==1&&left==0)priorityOperators++;
        }
        for(c in s){
            if(c=='('){
                left++
                positionLargestBracket==s.indexOf(c);
            }
            else if(c==')'&&left<=1){
                left--
                if(c==s[s.length-1]){
                    return calcString(s.substring(1,s.length-1))
                }
                var char=s.get(s.indexOf(c)+1)
                return callOperation(char,s.substring(1,s.indexOf(c)), s.substring((s.indexOf(c)+2)) )
            }
            else if(c==')'&&left>1){
                left--
            }
            else if(isOperator(c)==2&&priorityOperators==0){
                try{
                    priorityOperators--;
                    return callOperation(s.get(s.indexOf(c)),s.substring(0,s.indexOf(c)),s.substring(s.indexOf(c)+1) )
                }
                catch(e:Exception){

                }
            }
            else if(isOperator(c)==1&&left==0){
                return callOperation(s.get(s.indexOf(c)),s.substring(0,s.indexOf(c)),s.substring(s.indexOf(c)+1) )
            }
        }
        return s.toDouble()
    }
    private fun callOperation(operationChar:Char,first:String,second:String):Double{
        if(operationChar=='+'){
            return calcString(first)+calcString(second)
        }
        else if(operationChar=='-'){
            return calcString(first)-calcString(second)
        }
        else if(operationChar=='*'||operationChar=='('){
            return calcString(first)*calcString(second)
        }
        else if(operationChar=='/'){
            try{
                return calcString(first)/calcString(second)
            }
            catch(e:java.lang.Exception){
                return 0.toDouble();
            }

        }
        else{
            return -1.toDouble()
        }
    }
    private fun isOperator(s:Char):Int{
        if(s=='+'||s=='-'){
            return 1
        }
        else if(s=='/'||s=='*'){
            return 2
        }
        else{
            return 0
        }
    }

}